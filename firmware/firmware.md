# Прерывания
Прерывание выполняется 10 раз в секунду.
Его суть заключается в следующем.
Мы сохраняем значения регистров в стек, чтобы программа могла работать после прерывания.
Мы увеличиваем счётчик, чтобы выводить число цифр, соответствующее нашему дисплею.
Мы загружаем положение индикатора.

# Как бы я сделал прерывания.
Сохраняем значения регистров в стек.
Выключаем дисплей.
Достаём значение нужного символа.
Достаём значение нужного индикатора.
Выводим код символа и код цифры.
Увеличиваем счётчик.
Считываем значение кнопок.
```
Int610Hz:
    push r16	; saving registers in stack				; saving registers in stack
	in r16, SREG
	push r16
	push r17
	push r18
	push ZH
	push ZL

    ldi r16, 0b0011111      ; voiding display
    out PORTC, r16
    lds r16, 0b11111111
    out PORTD, r16
    
    ldi r16, r5
    ldi ZH,high(Symbols)   ; getting adress of symbol code by summing 
	ldi ZL,low(Symbols)
	ldi r17, 0				
	add ZL,r16          ; symbols list adress and symbol`s index adress
	adc ZH,r17

    ldi r16, Z          ; getting symbol code

    pop ZL	; putting register out of stack
    pop ZH
    pop r18
    pop r17
    pop r16
    out SREG, r16
    pop r16
    reti
```
Возвращаем значение регистров из стека.